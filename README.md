# Introduction

The objective of this workshop is to demonstrate how to integrate a Kubernetes cluster running on the Oracle Cloud Infrastructure into a multi-cloud Anthos environment:
![architecture.png](architecture/anthos.png)

We will not showcase all the Anthos-specific capabilitities that are provided on top of Istio, but instead focus on the multi-cluster capabilities that are provided out of the box by Istio, and create a multi-cluster service mesh between the Google Cloud Platform and the Oracle Cloud Infrastructure.

This workshop is adapted from this solution guide :
https://cloud.google.com/solutions/building-a-multi-cluster-service-mesh-on-gke-using-replicated-control-plane-architecture


# Pre-requisites
- Oracle Cloud and Google Cloud accounts
- oci CLI
- gcloud CLI
- kubectl and kubectx


# PART 1 - INSTALL CLUSTERS
In this section we will install the GKE and OKE clusters. The OKE cluster will be registered on Google Cloud using the GKE Hub.

### Cluster Installation
- create an OKE cluster with a single VM.Standard2.2 node and configure kubectl to access it (name the context 'oke')
- activate the necessary APIs on GCP:
```
gcloud auth login
export GCP_PROJECT=<your GCP project ID e.g. istio-demo-311411>
gcloud --project=$GCP_PROJECT services enable \
    cloudresourcemanager.googleapis.com \
    container.googleapis.com \
    gkeconnect.googleapis.com \
    gkehub.googleapis.com \
    serviceusage.googleapis.com \
    anthos.googleapis.com
```
- create a standard GKE cluster with release 1.19. For the pool, use 2 nodes with n1-standard-4 shapes (or a single node if you do not plan to do the optional sections)
- after the cluster is created, select it and click on "CONNECT" to display the gcloud command that needs to be executed to configure kubectl. By default the gke context will have a name in this format: `gke_${PROJECT_ID}_${CLUSTER_ZONE}_${CLUSTER_NAME}`. You can rename it `gke` with kubectx, e.g.:
```
kubectx gke=gke_$GCP_PROJECT_us-central1-c_gke
```

### Register the OKE cluster on GKE
- Create a "gkehub-connect" service account with the "GKE Connect Agent" role
- Generate JSON key for this service account and name it gkehub-connect-creds.json
- Use gcloud to register the cluster:
```
gcloud --project=$GCP_PROJECT container hub memberships register oke \
 --context=oke \
 --service-account-key-file=./gkehub-connect-creds.json 
```
- Manually reload the GCP console page in your browser and check you can see the OKE cluster in the GCP console
- Create a Kubernetes Service Account for the OKE cluster:
```
kubectl create serviceaccount oracle-admin-sa
kubectl create clusterrolebinding ksa-admin-binding \
    --clusterrole cluster-admin \
    --serviceaccount default:oracle-admin-sa
```
- Extract the token and use it to login from the GCP console:
```
printf "\n$(kubectl describe secret oracle-admin-sa | sed -ne 's/^token: *//p')\n\n"
```
- Click on the Workloads menu to check what is running on the OKE cluster


# PART 2 - INSTALL ISTIO
When deploying an Istio service mesh between multiple clusters, you need to decide if you want to use shared or separate networks and control planes. In this demo, we will use a multi-primary / multi-network setup (cluster1 represents the OKE cluster and cluster2 the GKE cluster):
![isio-install.png](architecture/istio-install.png)

Note that we tested the installation procedure with version 1.9.3 of Istio.


### Download Isio
```
export ISTIO_VERSION=1.9.3 && \
curl -L https://istio.io/downloadIstio | sh - && \
cd istio-$ISTIO_VERSION && \ 
export PATH=$PWD/bin:$PATH
```

### Prepare certificates for OKE
```
kubectx oke && \
kubectl create namespace istio-system && \
kubectl create secret generic cacerts -n istio-system \
    --from-file=samples/certs/ca-cert.pem \
    --from-file=samples/certs/ca-key.pem \
    --from-file=samples/certs/root-cert.pem \
    --from-file=samples/certs/cert-chain.pem
```

### Set the default Istio network for OKE
```
kubectl label namespace istio-system topology.istio.io/network=oke-network
```

### Prepare Istio install manifest for OKE
```
cat <<EOF > istio-oke.yaml
apiVersion: install.istio.io/v1alpha1
kind: IstioOperator
spec:
  values:
    global:
      meshID: mesh1
      multiCluster:
        clusterName: oke
      network: oke-network
  components:
    ingressGateways:
      - name: istio-eastwestgateway
        label:
          istio: eastwestgateway
          app: istio-eastwestgateway
          topology.istio.io/network: oke-network
        enabled: true
        k8s:
          env:
            # sni-dnat adds the clusters required for AUTO_PASSTHROUGH mode
            - name: ISTIO_META_ROUTER_MODE
              value: "sni-dnat"
            # traffic through this gateway should be routed inside the network
            - name: ISTIO_META_REQUESTED_NETWORK_VIEW
              value: oke-network
          service:
            ports:
              - name: status-port
                port: 15021
                targetPort: 15021
              - name: tls
                port: 15443
                targetPort: 15443
              - name: tls-istiod
                port: 15012
                targetPort: 15012
              - name: tls-webhook
                port: 15017
                targetPort: 15017
EOF
```

### Install Istio on OKE
```
istioctl install -y -f istio-oke.yaml
kubectl get all -n istio-system
```

### Create an Istio gateway that exposes all local services: 
```
kubectl apply -n istio-system -f \
    samples/multicluster/expose-services.yaml
kubectl get gateway -n istio-system
```

### Repeat all previous steps with GKE
```
kubectx gke && \ 
kubectl create namespace istio-system && \
kubectl create secret generic cacerts -n istio-system \
    --from-file=samples/certs/ca-cert.pem \
    --from-file=samples/certs/ca-key.pem \
    --from-file=samples/certs/root-cert.pem \
    --from-file=samples/certs/cert-chain.pem && \
kubectl label namespace istio-system topology.istio.io/network=gke-network

cat <<EOF > istio-gke.yaml
apiVersion: install.istio.io/v1alpha1
kind: IstioOperator
spec:
  values:
    global:
      meshID: mesh1
      multiCluster:
        clusterName: gke
      network: gke-network
  components:
    ingressGateways:
      - name: istio-eastwestgateway
        label:
          istio: eastwestgateway
          app: istio-eastwestgateway
          topology.istio.io/network: gke-network
        enabled: true
        k8s:
          env:
            # sni-dnat adds the clusters required for AUTO_PASSTHROUGH mode
            - name: ISTIO_META_ROUTER_MODE
              value: "sni-dnat"
            # traffic through this gateway should be routed inside the network
            - name: ISTIO_META_REQUESTED_NETWORK_VIEW
              value: gke-network
          service:
            ports:
              - name: status-port
                port: 15021
                targetPort: 15021
              - name: tls
                port: 15443
                targetPort: 15443
              - name: tls-istiod
                port: 15012
                targetPort: 15012
              - name: tls-webhook
                port: 15017
                targetPort: 15017
EOF

istioctl install -y -f istio-gke.yaml

kubectl get all -n istio-system

kubectl apply -n istio-system -f \
    samples/multicluster/expose-services.yaml
```

### Generate remote secrets
For the Istio control plane running on a given cluster to be able to discover services running on the remote cluster, a secret needs to be created. The task of generating a valid token creating the corresponding secret on OKE can be done automatically with `istioctl`:
```
istioctl x create-remote-secret --context=gke --name=gke | kubectl apply -f - --context=oke && \
istioctl x create-remote-secret --context=oke --name=oke | kubectl apply -f - --context=gke
```


# PART 3 - DEPLOY THE HIPSTER SHOP APPLICATION
We will deploy the hypster shop demo application accross the clusters of our service mesh (west cluster represents the OKE cluster and central cluster represents the GKE cluster):
![online-shop.png](architecture/online-shop.png)


### Get application manifests from Github
```
git clone https://github.com/GoogleCloudPlatform/istio-multicluster-gke.git
```

### Deploy the application on GKE
Note that we start the deployment on GKE because the loadgenerator service on OKE needs some services available on GKE to start properly.
```
kubectx gke && \
kubectl create namespace online-boutique && \
kubectl label namespace online-boutique istio-injection=enabled && \
kubectl -n online-boutique apply -f istio-multicluster-gke/istio-multi-primary/central

kubectl get all -n online-boutique
```

### Deploy the application on OKE
```
kubectx oke && \
kubectl create namespace online-boutique && \
kubectl label namespace online-boutique istio-injection=enabled && \
kubectl -n online-boutique apply -f istio-multicluster-gke/istio-multi-primary/west

kubectl get all -n online-boutique
```

### Access the application
Get the public IP addresses of the Istio ingress gateway on OKE:
```
kubectl --context oke get -n istio-system service istio-ingressgateway -o json | jq -r '.status.loadBalancer.ingress[0].ip'
```
Use this IP address to access the application:
![hipster-shop.png](architecture/hipster-shop.png)

Note that the application is fully functional, meaning in particular that the cart service on OKE can seamlessly access the checkout and payment services located on GKE. This is done thanks to mutual TLS authentication over the Internet through the EastWest Istio gateway!


# [OPTIONAL] PART 4 - MIGRATE THE ENTIRE APPLICATION TO GKE
- Get the public IP addresses of the Istio ingress gateway on GKE:
```
kubectl --context gke get -n istio-system service istio-ingressgateway -o json | jq -r '.status.loadBalancer.ingress[0].ip'
```
- Use this IP address to access the application from GKE and check it works well.

- Simulate a service failure on OKE:
```
kubectx oke && \
kubectl -n online-boutique delete -f istio-multicluster-gke/istio-multi-primary/west
```
- Check that the application has some issues (there is no frontend anymore, so you should see a "no healthy upstream" message)
- Redeploy the missing services on GKE:
```
kubectx gke && \
kubectl -n online-boutique apply -f istio-multicluster-gke/istio-multi-primary/west/deployments.yaml
```
- Check that the application works well again
 

# [OPTIONAL] PART 5 - MONITOR THE SERVICE MESH WITH ISTIO ADDONS
  
### Deploy Istio Addons
Deploy the Istio monitoring addons on the GKE cluster (re-execute the command if you have a Kiali error message):
```
kubectx gke && \
kubectl apply -f samples/addons

kubectl get all -n istio-system
```

### Check the service mesh performance with Grafana
- Setup port forwarding for Grafana:
```
kubectl -n istio-system port-forward \
    $(kubectl -n istio-system get pod -l app=grafana -o jsonpath='{.items[0].metadata.name}') 8080:3000
```
- Connect to http://localhost:8080/
- Go to Dashboards / Manage / istio folder and select the `Istio Mesh Dashboard`
- Check the availability and average latency of the services
- Cancel port forwarding with CTRL-C

### Visualize the service mesh with Kiali
- Setup port forwarding for Kiali:
```
kubectl -n istio-system port-forward \
    $(kubectl -n istio-system get pod -l app=kiali -o jsonpath='{.items[0].metadata.name}') 8080:20001
```
- Connect to http://localhost:8080/
- Go to `Graph` and select the `online-boutique` namespace
- Click on Display and select `Traffic Animation`
- Observe the communication flow between services


# CLEAN-UP
- Remove Online Boutique application and uninstall Istio:
```
kubectl --context=oke delete ns online-boutique && \
kubectl --context=gke delete ns online-boutique && \
istioctl --context=oke x uninstall -y --purge && \
kubectl --context=oke delete ns istio-system && \
istioctl --context=gke x uninstall -y --purge && \
kubectl --context=gke delete ns istio-system
```
- Delete the clusters from the OKE and GCP consoles

